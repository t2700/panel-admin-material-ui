import React from "react";
import {
  Box,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  ListItemIcon,
  Switch
} from "@mui/material";
import {Home,AccountBox,ModeNight,Storefront,Person,Settings,Pages,Groups} from "@mui/icons-material";

export default function Sidebar({setMode,mode}) {
  return (
    <Box
      // bgcolor="skyblue"
      
      flex={1}
      p={2}
      // sx={{ display: { xs: "none", sm: "block" } }}
    >
      <Box position="fixed">
      <List>
        <ListItem disablePadding>
          <ListItemButton component="a" href="#home">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <Home />
            </ListItemIcon>
            <ListItemText primary="HomePage" sx={{ display: { xs: "none", sm: "block" } }} />
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a" href="#simple-list">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <Pages />
            </ListItemIcon>
            <ListItemText primary="Pages" sx={{ display: { xs: "none", sm: "block" } }} />
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a" href="#simple-list">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <Groups />
            </ListItemIcon>
            <ListItemText primary="Groups" sx={{ display: { xs: "none", sm: "block" } }} />
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a" href="#simple-list">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <Storefront />
            </ListItemIcon>
            <ListItemText primary="Marketplace" sx={{ display: { xs: "none", sm: "block" } }}/>
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a" href="#simple-list">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <Person />
            </ListItemIcon>
            <ListItemText primary="Friends" sx={{ display: { xs: "none", sm: "block" } }}/>
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a" href="#simple-list">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <Settings />
            </ListItemIcon>
            <ListItemText  primary="Settings" sx={{ display: { xs: "none", sm: "block" } }} />
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a" href="#simple-list">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <AccountBox />
            </ListItemIcon>
            <ListItemText primary="Profile"  sx={{ display: { xs: "none", sm: "block" } }}/>
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a" href="#simple-list">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <ModeNight />
            </ListItemIcon>
          <Switch onChange={e=>setMode(mode ==="light"?"dark":"light")} sx={{ display: { xs: "block", sm: "block" } }}/>
          </ListItemButton>
        </ListItem>
      
      </List>
      </Box>
    </Box>
  );
}
