import React,{useState} from "react";
import {
  AppBar,
  Badge,
  InputBase,
  Box,
  styled,
  Toolbar,
  Typography,
  Avatar,
  Menu,
  MenuItem,
} from "@mui/material";
import { Pets, Mail,Notifications } from "@mui/icons-material/";

const StyledToolbar = styled(Toolbar)({
  display: "flex",
  justifyContent: "space-between",
});
const Search = styled("div")(({ theme }) => ({
  backgroundColor: "white",
  padding: "0 10px",
  borderRadius: theme.shape.borderRadius,
  width: "40%",
}));
const Icons = styled(Box)(({ theme }) => ({
    display:"none",
    gap:"20px",
    alignItems:'center',
    [theme.breakpoints.up("sm")]:{
        display:"flex"
    }
}));
const UserBox = styled(Box)(({ theme }) => ({
    display:"flex",
    gap:"10px",
    alignItems:'center',
    [theme.breakpoints.up("sm")]:{
        display:"none"
    }
}));
export default function Navbar() {
    const [open,setOpen]=useState(false)
  return (
    <AppBar position="sticky">
      <StyledToolbar>
       <Box gap={2} sx={{display:"flex"}}>
        <Pets sx={{ display: { xs: "block", sm: "block" } }} />
        <Typography variant="h6" sx={{ display: { xs: "none", sm: "block" ,} }}>
          Navbar{" "}
        </Typography>
        </Box>
        <Search>
          <InputBase placeholder="search..." />
        </Search>
        <Icons>
          <Badge badgeContent={4} color="error">
            <Mail />
          </Badge>
          <Badge badgeContent={4} color="error">
            <Notifications />
          </Badge>
          <Avatar
           sx={{width:30,height:30}} 
          src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6IiBSq-HdVj8qnaiRUVLK9WALClkzwzyAzw&usqp=CAU"
          onClick={(e)=>setOpen(true)}
          />
        </Icons>
        <UserBox onClick={(e)=>setOpen(true)}>
        <Avatar
           sx={{width:30,height:30}} 
          src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6IiBSq-HdVj8qnaiRUVLK9WALClkzwzyAzw&usqp=CAU"/>
        </UserBox>
        <Typography variant="span" sx={{ display: { xs: "block", sm: "none" } }}>John</Typography>
      </StyledToolbar>
      <Menu
        id="basic-menu"
        open={open}
        onClose={e=>setOpen(false)}
       anchorOrigin={{
           vertical:"top",
           horizontal:"right"
       }}
       transformOrigin={{
           vertical:"top",
           horizontal:'right'
       }}
      >
        <MenuItem>Profile</MenuItem>
        <MenuItem>My account</MenuItem>
        <MenuItem>Logout</MenuItem>
      </Menu>
    </AppBar>
  );
}
